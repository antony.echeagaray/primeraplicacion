package com.example.primeraplicacion;

import java.io.Serializable;
import java.util.Random;

public class cotizacion implements Serializable {

    private int folio;

    private String descripcion;

    private float valorAuto;

    private float porcentajePagoInicial;

    private int plazos;

    public cotizacion(int folio, String descripcion, float valorAuto, float porcentajePagoInicial, int plazos) {
        this.folio = folio;
        this.descripcion = descripcion;
        this.valorAuto = valorAuto;
        this.porcentajePagoInicial = porcentajePagoInicial;
        this.plazos = plazos;
    }

    public cotizacion() {

    }

    public int getFolio() {
        return folio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public float getValorAuto() {
        return valorAuto;
    }

    public float getPorcentajePagoInicial() {
        return porcentajePagoInicial;
    }

    public int getPlazos() {
        return plazos;
    }

    public void setFolio(int folio) {
        this.folio = folio;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setValorAuto(float valorAuto) {
        this.valorAuto = valorAuto;
    }

    public void setPorcentajePagoInicial(float porcentajePagoInicial) {
        this.porcentajePagoInicial = porcentajePagoInicial;
    }

    public void setPlazos(int plazos) {
        this.plazos = plazos;
    }

    //Metodos de COmportamiento

    public int generarID(){
        Random r = new Random();
        return r.nextInt(1000);
    }

    public float calcularPagoInicial(){
        return this.valorAuto * (this.porcentajePagoInicial/100);
    }

    public float calcularPagoMensual(){
        return (this.valorAuto-this.calcularPagoInicial())/this.plazos;
    }
}

