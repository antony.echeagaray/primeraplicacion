package com.example.primeraplicacion;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;

public class convMonedas extends AppCompatActivity {

    private EditText editTextCantidad;
    private Spinner spinnerMonedas;
    private Button btnCalcular, btnLimpiar, btnCerrar;
    private TextView textViewResultado;

    private final double USD_RATE = 0.059;
    private final double EUR_RATE = 0.055;
    private final double CAD_RATE = 0.081;
    private final double GBP_RATE = 0.046;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conv_monedas);

        editTextCantidad = findViewById(R.id.editTextCantidad);
        spinnerMonedas = findViewById(R.id.spinnerMonedas);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnCerrar = findViewById(R.id.btnCerrar);
        textViewResultado = findViewById(R.id.textViewResultado);

        // Configurar el Spinner con las opciones de monedas
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.monedas_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerMonedas.setAdapter(adapter);

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calcularConversion();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiarCampos();
            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void calcularConversion() {
        String cantidadStr = editTextCantidad.getText().toString();
        if (cantidadStr.isEmpty()) {
            Toast.makeText(this, "Por favor, ingrese una cantidad", Toast.LENGTH_SHORT).show();
            return;
        }

        double cantidad = Double.parseDouble(cantidadStr);
        String monedaSeleccionada = spinnerMonedas.getSelectedItem().toString();
        double resultado = 0;

        switch (monedaSeleccionada) {
            case "Dólares Americanos":
                resultado = cantidad * USD_RATE;
                break;
            case "Euros":
                resultado = cantidad * EUR_RATE;
                break;
            case "Dólares Canadienses":
                resultado = cantidad * CAD_RATE;
                break;
            case "Libras Esterlinas":
                resultado = cantidad * GBP_RATE;
                break;
        }

        textViewResultado.setText(String.format("Resultado: %.2f %s", resultado, monedaSeleccionada));
    }

    private void limpiarCampos() {
        editTextCantidad.setText("");
        spinnerMonedas.setSelection(0);
        textViewResultado.setText("Resultado");
    }
}
