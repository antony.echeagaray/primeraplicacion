package com.example.primeraplicacion;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import org.w3c.dom.Text;

public class cotizacionActivity extends AppCompatActivity {

    private TextView txtUsuario;
    private RadioGroup radiogroup;
    private TextView txtFolio;
    private EditText txtDescription;
    private EditText txtValorAuto;
    private EditText txtporcentaje;
    private RadioButton rdb12;
    private RadioButton rdb18;
    private RadioButton rdb24;
    private RadioButton rdb30;
    private TextView txtPagoMensual;
    private TextView txtPagoInicial;
    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnRegresar;
    private cotizacion cot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_cotizacion);

        txtUsuario = findViewById(R.id.txtUsuario);
        txtFolio = findViewById(R.id.txtFolio);
        txtDescription = findViewById(R.id.txtDescripcion);
        txtValorAuto = findViewById(R.id.txtValorAuto);
        txtporcentaje = findViewById(R.id.txtporcentaje);
        radiogroup = findViewById(R.id.radiogroup);
        rdb12 = findViewById(R.id.rdb12);
        rdb18 = findViewById(R.id.rdb18);
        rdb24 = findViewById(R.id.rdb24);
        rdb30 = findViewById(R.id.rdb30);
        txtPagoMensual = findViewById(R.id.txtPagoMensual);
        txtPagoInicial = findViewById(R.id.txtPagoInicial);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnRegresar = findViewById(R.id.btnRegresar);
        btnLimpiar = findViewById(R.id.btnLimpiar);

        cot = new cotizacion();
        txtFolio.setText("Folio:" + String.valueOf((cot.generarID())));
        Bundle datos = getIntent().getExtras();
        String nombre = datos.getString("cliente");
        txtUsuario.setText("Cliente: " + nombre);


        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtDescription.getText().toString().matches("") || txtValorAuto.getText().toString().matches("") || txtporcentaje.getText().toString().matches("")) {
                    Toast.makeText(cotizacionActivity.this, "Falto capturar informacion", Toast.LENGTH_SHORT).show();
                    txtDescription.requestFocus();
                } else {
                    try {
                        float valorAuto = Float.parseFloat(txtValorAuto.getText().toString());
                        float porcentajePagoInicial = Float.parseFloat(txtporcentaje.getText().toString());

                        if (valorAuto <= 0 || porcentajePagoInicial <= 0) {
                            Toast.makeText(cotizacionActivity.this, "Valores deben ser positivos", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        int plazos = 0;
                        float enganche = 0.0f;
                        float pagoMensual = 0.0f;

                        if (rdb12.isChecked()) {
                            plazos = 12;
                        }
                        if (rdb18.isChecked()) {
                            plazos = 18;
                        }
                        if (rdb24.isChecked()) {
                            plazos = 24;
                        }
                        if (rdb30.isChecked()) {
                            plazos = 36;
                        }

                        cot.setDescripcion(txtDescription.getText().toString());
                        cot.setValorAuto(valorAuto);
                        cot.setPorcentajePagoInicial(porcentajePagoInicial);
                        cot.setPlazos(plazos);

                        enganche = cot.calcularPagoInicial();
                        pagoMensual = cot.calcularPagoMensual();

                        txtPagoInicial.setText("Pago Inicial $" + String.valueOf(enganche));
                        txtPagoMensual.setText("Pago Mensual $" + String.valueOf(pagoMensual));
                    } catch (NumberFormatException e) {
                        Toast.makeText(cotizacionActivity.this, "Por favor ingrese valores válidos", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });


        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtDescription.setText("");
                txtValorAuto.setText("");
                txtporcentaje.setText("");
                txtPagoMensual.setText("Pago Mensual: ");
                radiogroup.clearCheck();
            }
        });




        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }
}