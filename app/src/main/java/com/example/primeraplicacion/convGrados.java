package com.example.primeraplicacion;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;

import com.example.primeraplicacion.R;

public class convGrados extends AppCompatActivity {

    private EditText editTextTemperature;
    private RadioGroup radioGroup;
    private RadioButton radioButtonCelsius, radioButtonFahrenheit;
    private Button btnCalcular, btnLimpiar, btnCerrar;
    private TextView txtResultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conv_grados); // Asegúrate de que esto coincida con el nombre correcto de tu layout

        editTextTemperature = findViewById(R.id.editTextTemperature);
        radioGroup = findViewById(R.id.radioGroup);
        radioButtonCelsius = findViewById(R.id.radioButtonCelsius);
        radioButtonFahrenheit = findViewById(R.id.radioButtonFahrenheit);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnCerrar = findViewById(R.id.btnCerrar);
        txtResultado = findViewById(R.id.txtResultado);

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                convertTemperature();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearFields();
            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void convertTemperature() {
        String input = editTextTemperature.getText().toString();
        if (input.isEmpty()) {
            txtResultado.setText("Por favor, ingrese una temperatura");
            return;
        }

        double temperature = Double.parseDouble(input);
        double convertedTemperature;
        String resultText;

        if (radioButtonCelsius.isChecked()) {
            convertedTemperature = (temperature * 9 / 5) + 32;
            resultText = String.format("%s Celsius son %.2f Fahrenheit", temperature, convertedTemperature);
        } else if (radioButtonFahrenheit.isChecked()) {
            convertedTemperature = (temperature - 32) * 5 / 9;
            resultText = String.format("%s Fahrenheit son %.2f Celsius", temperature, convertedTemperature);
        } else {
            resultText = "Por favor, seleccione un tipo de conversión";
        }

        txtResultado.setText(resultText);
    }

    private void clearFields() {
        editTextTemperature.setText("");
        radioGroup.clearCheck();
        txtResultado.setText("Resultado");
    }
}
